const functions = require("firebase-functions");
const app = require('./src/app');

exports.Api = functions.https.onRequest(app);
