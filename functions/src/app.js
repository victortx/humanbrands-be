const express = require('express');
const bodyParser = require('body-parser');
const router = require('./network/routers');
const cors = require('cors');

const app = express();
app.listen(3000);
app.use(cors());
app.use(bodyParser.json());

router(app);
// se definen las rutas
module.exports = app;
