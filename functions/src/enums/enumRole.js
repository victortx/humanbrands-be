const USER_ROLE = {
    USER: 'USUARIO',
    SUPER_USER: 'SUPER_USUARIO',
}

module.exports = USER_ROLE;
