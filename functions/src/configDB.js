const admin = require('firebase-admin');
const firebaseDB = require('firebase/app');

const config = {
    apiKey: "AIzaSyDkIbQMn-YDcmQw9I71jJXj7gatOJdmQEA",
    authDomain: "humanbrands-be.firebaseapp.com",
    projectId: "humanbrands-be",
    storageBucket: "humanbrands-be.appspot.com",
    messagingSenderId: "199852043595",
    appId: "1:199852043595:web:cb981361001dbcca1c0cce"
}

const serviceAccount = require('./credentials.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
});

firebaseDB.initializeApp(config);

module.exports = {
    configDB: admin,
    firebaseDB,
};
