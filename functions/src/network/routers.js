// export components
const auth = require('../components/auth/router');
const user = require('../components/usuarios/router');
const noticia = require('../components/noticias/router');
const video = require('../components/video/router');
const checkAuthenticated = require('../utils/Authenticated');

const routes = (server) => {
    // se define los componentes con las rutas
    server.use('/auth', auth);
    server.use('/user', checkAuthenticated , user);
    server.use('/news', noticia);
    server.use('/video', video);
}

module.exports = routes;
