const { configDB } = require('../configDB');
const response = require('../network/response');

// middleware authenticated
async function checkIfAuthenticated  (req, res, next) {
    try {
        // checking if it brings the authorizacion headers
        if(!req.headers.authorization){
            return response.error(req, res, 'No contiene cabezera de autenticacion', 404);
        }
        // serialize token -> idToken
        const tokenSerialize = req.headers.authorization.replace(/^Bearer\s/, '');
        // verify token user
        await configDB.auth().verifyIdToken(tokenSerialize)
        return next();
    } catch (e) {
        return response.error(req, res, 'Sesión  expirada', 401);
    }
}

module.exports = checkIfAuthenticated;
