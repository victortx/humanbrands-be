const { configDB } = require('../../configDB');
const firebaseAuth = require('firebase/auth');
const firebaseDB = configDB.firestore();

async function login(userParams) {
        if(!userParams.hasOwnProperty('email') || !userParams.hasOwnProperty('password')){
            return Promise.reject('Error en los datos');
        }
        const auth = firebaseAuth.getAuth();
        let messageError = '';
        // autenticacion con Firebase
        const resultAuth = await firebaseAuth.signInWithEmailAndPassword(auth, userParams.email, userParams.password)
            .then(
                result => {
                    return result['_tokenResponse'];
                })
            .catch(e => {
            messageError = e;
        });

        if(messageError !== '') {
            return Promise.reject('Error en el correo o contraseña');
        }

        let messageErrorUser ;

        const result = await firebaseDB.collection('users')
            .where('email', '==', resultAuth['email'])
            .get()
            .then((data) => {
                return data.docs.map(doc => doc.data());
            }).catch(e => {
                messageErrorUser = e;
            });

        if(messageErrorUser !== undefined) {
            return Promise.reject('Error al obtener los datos del usuario');
        }
        // delete
        delete resultAuth['kind'];
        delete resultAuth['localId'];
        delete resultAuth['registered'];
        // set result auth
        resultAuth['idUser'] = result[0]['id'];
        resultAuth['name'] = result[0]['name'];

    return Promise.resolve(resultAuth);
}

module.exports = {
    login,
}
