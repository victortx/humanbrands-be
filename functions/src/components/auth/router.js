const express = require('express');
const response = require('../../network/response');
const controller = require('./controller');

const router = express.Router();

router.post('/login', (req, res) => {
    // Login user
    controller.login(req.body).then(data => {
        response.success(req, res, data, 200);
    }).catch(e => {
        response.error(req, res, e, 500);
    })

});


module.exports = router;
