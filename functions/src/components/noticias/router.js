const express = require('express');
const controller = require('./controller');
const response = require('../../network/response');
const checkAuthenticated = require('../../utils/Authenticated');

const router = express.Router();

router.post('/', checkAuthenticated, (req, res) => {
    controller.add(req.body).then(data => {
        response.success(req, res, data, 201);
    }).catch(e => {
        response.error(req, res, e, 500);
    });
});

router.get('/', (req, res) => {
   controller.all().then(data => {
       response.success(req, res, data, 200);
   }).catch(e => {
       response.error(req, res, e, 500);
   });
});

router.get('/:id', (req, res) => {
    controller.getById(req.params.id).then(data => {
        response.success(req, res, data, 200);
    }).catch(e => {
        response.error(req, res, e, 500);
    });
});

router.put('/:id', checkAuthenticated, (req, res) => {
   controller.update(req.params.id, req.body).then(data => {
       response.success(req, res, data, 200);
   }).catch(e => {
       response.error(req, res, e, 500);
   })
});

router.delete('/:id', checkAuthenticated, (req, res) => {
   controller.delete(req.params.id).then(data => {
       response.success(req, res, data, 200);
   }).catch(e => {
       response.error(req, res, e, 500);
   })
});

module.exports = router;
