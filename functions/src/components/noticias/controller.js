const { configDB }= require('../../configDB');
const db = configDB.firestore();
const nameCollection = 'news';


async function agregarNotias(dataNews) {

    if(!dataNews.hasOwnProperty('idUser') || !dataNews.hasOwnProperty('description')
        || !dataNews.hasOwnProperty('title')) {
        return Promise.reject('Falta de datos');
    }


    if(validateData(dataNews)){
        return Promise.reject('Error de datos');
    }
    const newsSerialize = serializerNews(dataNews);

    let messageError = '';

    const dataResult = await db.collection(nameCollection)
        .add(newsSerialize)
        .then(async data => {
            // set id
            newsSerialize['id'] = data['id'];
            await db.collection(nameCollection)
                .doc(data['id'])
                .update(newsSerialize);
        }).catch(e => messageError = e);

    if(messageError.length !== 0) {
        return Promise.reject('Error al crear las noticas');
    }

    return Promise.resolve(newsSerialize);
}

async function getNoticias() {
    const result = await db.collection(nameCollection).get();
    return result.docs.map(doc => doc.data());
}

async function getNoticiaById(id) {
    const noticiaResult =  await db.collection(nameCollection)
        .doc(id)
        .get()
        .then((data) => {
            return data.data();
        });
    if (noticiaResult === undefined) return Promise.reject();
    return Promise.resolve(noticiaResult);
}

async function updateNoticia(id, dataParams) {

    if(id === '' || id === undefined){
        return Promise.reject('Falta identificador');
    }

    if(validateData(dataParams)){
        return Promise.reject('Error de datos');
    }

    const newsSerialize = serializerNews(dataParams);
    newsSerialize['id'] = id;
    await db.collection(nameCollection)
        .doc(newsSerialize['id'])
        .update(newsSerialize);

    return Promise.resolve(newsSerialize);
}

async function deleteNoticia(id) {
    const result = await db.collection(nameCollection)
        .doc(id)
        .delete()
        .then(() => {
            return true;
        }).catch(e => {
            return false;
        });
    if(result) return Promise.resolve('');
    return Promise.reject('Error al eliminar el registro')
}

const validateData = data =>
    !data.idUser ||
    !data.description ||
    !data.title;


function serializerNews(params) {
    return {
        idUser: params.idUser,
        description: params.description,
        title: params.title
    };
}



module.exports = {
    add: agregarNotias,
    all: getNoticias,
    getById: getNoticiaById,
    update: updateNoticia,
    delete: deleteNoticia,
}
