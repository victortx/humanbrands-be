const { configDB }= require('../../configDB');
const db = configDB.firestore();
const nameCollection = 'video';

async function createVideo(dataParams) {
    if(!dataParams.hasOwnProperty('idUser') || !dataParams.hasOwnProperty('description')
        || !dataParams.hasOwnProperty('title') || !dataParams.hasOwnProperty('videoUrl')) {
        return Promise.reject('Falta de datos');
    }

    if(validateVideoData(dataParams)){
        return Promise.reject('Falta de campos requeridos');
    }

    const dataVideo = parseVideoData(dataParams);

    let messageError = '';

    const dataResult = await db.collection(nameCollection)
        .add(dataVideo)
        .then(async data => {
            // set id
            dataVideo['id'] = data['id'];
            await db.collection(nameCollection)
                .doc(data['id'])
                .update(dataVideo);
        }).catch(e => messageError = e);

    if(messageError.length !== 0) {
        return Promise.reject('Error al crear el video');
    }

    return Promise.resolve(dataVideo);
}

async function updateVideo(id, dataParams) {
    if(id === '' || id === undefined){
        return Promise.reject('Falta identificador');
    }

    if(validateVideoData(dataParams)){
        return Promise.reject('Error de datos');
    }

    const videoSerialize = parseVideoData(dataParams);
    videoSerialize['id'] = id;
    await db.collection(nameCollection)
        .doc(videoSerialize['id'])
        .update(videoSerialize);
    return Promise.resolve(videoSerialize);
}

async function getVideoById(id) {
    const videoResult =  await db.collection(nameCollection)
        .doc(id)
        .get()
        .then((data) => {
            return data.data();
        });
    if (videoResult === undefined) return Promise.reject();
    return Promise.resolve(videoResult);

}

async function getVideos() {
    const result = await db.collection(nameCollection).get();
    return result.docs.map(doc => doc.data());
}

async function videoDelete(id) {
    const result = await db.collection(nameCollection)
        .doc(id)
        .delete()
        .then(() => {
            return true;
        }).catch(e => {
            return false;
        });
    if(result) return Promise.resolve('');
    return Promise.reject('Error al eliminar el registro')
}

function parseVideoData(introData) {
    return {
        idUser: introData.idUser,
        description: introData.description,
        title: introData.title,
        videoUrl: introData.videoUrl,
    };
}

const validateVideoData = data =>
    !data.idUser ||
    !data.description ||
    !data.title ||
    !data.videoUrl;

module.exports = {
    add: createVideo,
    getById: getVideoById,
    get: getVideos,
    update: updateVideo,
    delete: videoDelete,
}
