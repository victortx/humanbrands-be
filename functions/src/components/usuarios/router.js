const express = require('express');
const response = require('../../network/response');
const controller = require('./controller');

const router = express.Router();

router.post('/', (req, res) => {
    // controller create user
    controller.add(req.body).then(data => {
        response.success(req, res, data, 201);
    }).catch(e => {
       response.error(req, res, e,500);
    });
});

router.get('/', (req, res) => {
    // all user
    controller.allUser().then(data => {
        response.success(req, res, data, 200);
    }).catch(e => {
        response.error(req, res, e, 404);
    })
})

router.get('/:id', (req, res) => {
    // get user by id
    controller.getById(req.params.id).then(data => {
        response.success(req, res, data, 200);
    }).catch(e => {
        response.error(req, res, 'No se encontro los datos del usuario', 404);
    });
});

router.put('/', (req, res) => {
    // update user
    controller.updateUser(req.body).then(data => {
        response.success(req, res, data, 200);
    }).catch(e => {
        response.error(req, res, e, 404);
    });
});

router.delete('/:id', (req, res) => {
   //change status user
   controller.deactivateUser(req.params.id).then( data => {
       response.success(req, res, data, 200);
   }).catch(e => {
       response.error(req, res, e, 500);
   });
});


module.exports = router;
