const { configDB }= require('../../configDB');
const db = configDB.firestore();
const nameCollection = 'users';
const enums = require('../../enums/enumRole');

async function CreateUser(dataUser) {
    // validate data params
    if (validateDataUser(dataUser))
    {
        return Promise.reject('Falta de campos requeridos');
    }
    const user = parseUser(dataUser);

    // validate email exist
    const emailExist = await db.collection(nameCollection).where('email', '==', user.email).get();
    if (emailExist.docs.map(doc => doc.data()).length > 0) {
        return Promise.reject('El correo ya existe');
    }
    const propertiesDataUser = { "password": dataUser.password, "email": user.email};
    let errorMessage = '';

    // create auth, email and password
    const { uid } = await configDB.auth().createUser(propertiesDataUser)
        .catch((error) => {
            errorMessage = error.message;
        });


    if (errorMessage.length !== 0) {
        return Promise.reject('Error al crear el usuario');
    }

    dataUser["emailId"] = uid;
    delete dataUser['password'];
    // save users in firestore
    await db.collection(nameCollection)
        .add(dataUser)
        .then(async (data) => {
            dataUser['id'] = data['id'];
            await db.collection(nameCollection).doc(data['id']).update(dataUser);
        });
    return Promise.resolve(dataUser);
}

async function getUser(id) {
    const userData =  await db.collection(nameCollection)
        .doc(id)
        .get()
        .then((data) => {
            return data.data();
        });
    if (userData === undefined) return Promise.reject();
    return Promise.resolve(userData);
}

async function getAllUser() {
    const response = await db.collection(nameCollection).get();
    return response.docs.map(doc => doc.data());
}

async function updateUser(dataUser) {

    if(validateUpdateUser(dataUser)){
        return Promise.reject('Falta de campos requeridos');
    }

    const result = await db.collection(nameCollection)
        .doc(dataUser.id)
        .update(dataUser);

    if(result === undefined) {
        return Promise.reject('Error al actualizar los datos del usuario');
    }

    return Promise.resolve(dataUser);
}

async function deactivateUser(id) {

    const dataUser = await getUser(id)
        .then(
            data => {
                return data
            }).catch(e => { return undefined });

    if(dataUser === undefined){
        return Promise.reject('No se encontro al usuario');
    }

    const response = await db.collection(nameCollection).doc(id).update({ "status": false });
    // validate response update
    if(response === undefined){
        return Promise.reject('Error al desactivar el usuario');
    }

    const responseAuthenticate = await configDB.auth()
        .updateUser(dataUser.emailId, {disabled: true})
        .then(e => { return true })
        .catch(e => { return false })

    if(!responseAuthenticate) return Promise.reject('Error al desactivar al usuario');

    return Promise.resolve('Se ha desactivado al usuario');

}

const validateUpdateUser = data =>
    !data.id ||
    !data.name ||
    !data.email ||
    !data.status ||
    !data.role;

const validateDataUser = data =>
    !data.name ||
    !data.email ||
    !data.password ||
    !data.role ||
    !data.status;

function parseUser(introData) {
    return {
      name: introData.name,
      email: introData.email,
      role: introData.role,
      status: introData.status,
    };
}


module.exports = {
    add: CreateUser,
    getById: getUser,
    allUser: getAllUser,
    updateUser: updateUser,
    deactivateUser: deactivateUser,
}

